//
// Created by ww on 2019-05-15.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation
import XCTest

@testable import Api

class ApiClientTests: XCTestCase {

    var apiClient = ApiClient.shared
    let mockSession = URLSessionMock()

    let mockUrl = "https://mock.url"

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRequestWithURL() {
        let urlRequest = URLRequest(urlString: mockUrl, httpMethod: .get)

        apiClient.urlSession = mockSession
        apiClient.request(urlRequest: urlRequest!, completion: { data in
            // Return data
        })

        XCTAssert(mockSession.lastURL?.absoluteString == mockUrl)
    }

    func testRequestResumeCalled() {
        let urlRequest = URLRequest(urlString: mockUrl, httpMethod: .get)
        let dataTask = URLSessionDataTaskMock()
        mockSession.dataTask = dataTask

        apiClient.urlSession = mockSession
        apiClient.request(urlRequest: urlRequest!, completion: { data in
            // Return data
        })

        XCTAssert(dataTask.resuming)
    }

    func testRequestReturnsData() {
        let urlRequest = URLRequest(urlString: mockUrl, httpMethod: .get)
        mockSession.data = "{}".data(using: .utf8)

        apiClient.urlSession = mockSession
        apiClient.request(urlRequest: urlRequest!, completion: { data in
            XCTAssertNotNil(data)
        })
    }
}
