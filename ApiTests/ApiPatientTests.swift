//
// Created by ww on 2019-05-15.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation
import XCTest

@testable import Api

class ApiPatientTests: XCTestCase {

    var apiPatient = ApiPatient.shared
    let mockSession = URLSessionMock()

    let mockUrl = "https://mock.url"

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testGetPatientWithId() {
        let patientId = "123456"
        let actualJson = """
                         {
                              "id": "123456",
                              "firstName": "Joe",
                              "lastName": "Black",
                              "email": "joe@black.com",
                              "dob": "1970-04-09 22:00:00 +0000"
                         }
                         """
        let actualPatient = JSONDecoder().decode(type: PatientModel.self, json: actualJson)

        mockSession.data = actualJson.data(using: .utf8)
        apiPatient.urlSession = mockSession

        apiPatient.post(patientId: patientId, completion: { patient in
            XCTAssertNotNil(patient)
            XCTAssertEqual(actualPatient.id, patient.id)
        })
    }
}
