//
//  SecondViewController.swift
//  Api
//
//  Created by ww on 2019/05/14.
//  Copyright (c) 2019 Infinity. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    // MARK: - Properties

    private var apiPatient = ApiPatient.shared
    private var authClient = AuthClient.shared

    var patient: Patient?

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        autoSignIn()
    }

    // MARK: - Operations

    func autoSignIn() {
        let index = 0
        let emails = [
            "storm@light.com",
            "jo@black.com"
        ]
        let pass = [
            "Sl123456",
            "Jb123456"
        ]

        signIn(email: emails[index], password: pass[index])
    }

    private func signIn(email: String, password: String) {
        self.showActivityIndicator()
        authClient.signIn(
                email: email,
                password: password,
                completion: { credentials in
                    self.apiPatient.get(completion: { patient in
                        print("patient: \(patient)")
                        self.hideActivityIndicator(mainThread: true)
                    }, failure: { error in
                        self.hideActivityIndicator(mainThread: true)
                    })
                },
                failure: { error in
                    self.hideActivityIndicator(mainThread: true)
                    if let error = error {
                        print("\(error.localizedDescription)")
                    }
                }
        )
    }

    func getPatient(id: String) {
        self.apiPatient.get(patientId: id, completion: { patient in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "thirdVC", sender: patient)
            }
        }, failure: { error in })
    }

}

