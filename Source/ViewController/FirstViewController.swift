//
//  FirstViewController.swift
//  Api
//
//  Created by ww on 2019/05/14.
//  Copyright (c) 2019 Infinity. All rights reserved.
//

import UIKit

// MARK: - Action
// MARK: - Outlet
// MARK: - Properties
// MARK: - Init
// MARK: - View life cycle
// MARK: - Setup
// MARK: - Data
// MARK: - Operations
class FirstViewController: UIViewController, DataDelegate {

    // MARK: - Properties

    var patient: Patient?

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "thirdVC" {
            let thirdVC = segue.destination as! ThirdViewController
            thirdVC.patient = sender as? Patient
            thirdVC.delegate = self
        }
    }

    // MARK: - Data

    func didReceiveData(data: Any) {
        print("FirstViewController/didReceiveData: \(data)")
    }
}

