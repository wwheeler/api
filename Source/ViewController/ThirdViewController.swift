//
//  ThirdViewController.swift
//  Api
//
//  Created by ww on 2019/05/14.
//  Copyright (c) 2019 Infinity. All rights reserved.
//

import UIKit

protocol DataDelegate: class {
    func didReceiveData(data: Any)
}

class ThirdViewController: UIViewController {

    // MARK: - Action

    @IBAction func save(_ sender: UIButton) {
        saveAction()
    }

    // MARK: - Properties

    weak var delegate: DataDelegate?

    var patient: Patient?

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        if let patient = patient {
            print("ThirdViewController/patient: \(patient)")
        }
    }

    // MARK: - Operations

    private func saveAction() {
        if var updatePatient = patient {
            updatePatient.firstName = "Joe"
            delegate?.didReceiveData(data: updatePatient)
            navigationController?.popViewController(animated: true)
        }
    }
}
