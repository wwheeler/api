//
// Created by ww on 2019-05-16.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

extension JSONEncoder {

    func encode<T>(value: T) -> Data? where T: Encodable {
        let jsonEncoder = self
        jsonEncoder.dateEncodingStrategy = .formatted(Date.formatter(with: Formats.yyyy_MM_dd))
        jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
        do {
            return try jsonEncoder.encode(value)
        } catch {
            print("> JSONEncoder/error: \(value) \(error)")
            return nil
        }
    }

}