//
// Created by ww on 2019/04/23.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

extension Date {

    static func date(from string: String, format: String) -> Date {
        let dateFormat = self.formatter(with: format)
        return dateFormat.date(from: string)!
    }

    static func formatter(with format: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        return dateFormatter
    }

    func getAge() -> Int {
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: self, to: now)
        return ageComponents.year!
    }

    mutating func setTimeToZero() {
        let cal = Calendar(identifier: .gregorian)
        var components = cal.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0

        self = cal.date(from: components)!
    }

    func advanceBy(_ value: Int) -> Date? {
        let day = self
        let cal = Calendar.current as NSCalendar
        if let next = cal.date(byAdding: .day, value: value, to: day, options: NSCalendar.Options(rawValue: 0)) {
            return next
        }

        return nil
    }
}
