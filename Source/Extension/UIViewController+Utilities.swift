//
// Created by ww on 2019/04/23.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import UIKit

extension UIViewController {

    func showActivityIndicator() {
        ActivityIndicator.showActivityIndicator(uiView: self.view)
    }

    func hideActivityIndicator(mainThread: Bool? = false){
        if mainThread! {
            DispatchQueue.main.async {
                ActivityIndicator.hideActivityIndicator(uiView: self.view)
            }
        } else {
            ActivityIndicator.hideActivityIndicator(uiView: self.view)
        }
    }

}
