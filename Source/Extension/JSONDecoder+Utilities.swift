//
// Created by ww on 2019-05-16.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

extension JSONDecoder {

    func decode<T>(type: T.Type, data: Data) -> T? where T: Decodable {
        let jsonDecoder = self
        jsonDecoder.dateDecodingStrategy = .formatted(Date.formatter(with: Formats.yyyy_MM_dd))
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            return try jsonDecoder.decode(type, from: data)
        } catch {
            print("> JSONDecoder/error: \(type) \(error)")
            return nil
        }
    }

}
