//
// Created by ww on 2019/04/27.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

extension URLRequest {

    init?(
            urlString: String,
            urlParameters: [String: String]? = nil,
            httpMethod: HttpMethod,
            httpHeaders: [String: String]? = nil,
            httpBody: Data? = nil
    ) {
        var urlPath: String = urlString + (urlParameters != nil ? "?" : "")
        if let parameters = urlParameters {
            urlPath.append(params: parameters)
        }

        if let url = URL(string: urlPath) {
            print("> \(urlPath)")
            self.init(url: url, method: httpMethod, headers: httpHeaders, body: httpBody)
        } else {
            return nil
        }
    }

    init(
            url: URL,
            method: HttpMethod,
            headers: [String: String]? = nil,
            body: Data? = nil
    ) {
        self.init(url: url)

        if let headers = headers {
            for (header, value) in headers {
                addValue(value, forHTTPHeaderField: header)
            }
        }

        httpMethod = method.rawValue
        httpBody = body
    }
}
