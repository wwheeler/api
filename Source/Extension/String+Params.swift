//
// Created by ww on 2019/04/16.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

extension String {
    mutating func append(params: [String: String]) {
        for (key, value) in params {
            self += "\(key)=\(value)&"
        }
    }

    func appending(params: [String: String]) -> String {
        var string = self
        string.append(params: params)
        return string
    }
}
