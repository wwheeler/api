//
// Created by wwheeler on 2019/04/12.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

// swiftlint:disable nesting
struct Api {

    static let server = "\(environment)"
    private static let production = "https://sympliact-rest-api-staging.herokuapp.com/api/v1"
    private static let staging = "https://sympliact-rest-api-staging.herokuapp.com/api/v1"
    private static let qa = "https://sympliact-rest-api-staging.herokuapp.com/api/v1"
    private static var environment: String {
        switch Environment.active {
        case .production: return production
        case .staging: return staging
        case .qa: return qa
        }
    }

    struct Keys {
        static let acuantMobile: String = "359FB52E46CC"
        static let backendless: String = "29568C7F-2106-748D-FF24-82248D0D5400"
        static let appId: String = "93593F69-5DCC-BC37-FF43-D2B5F0317600"
    }

    struct Paths {
        static let signIn = "/signin"

        static let pat = "/pat"

        static let users = "/users/"
        static let profiles = "/profile/"
        static let patients = "/patients/"

        static let searchMedicines = "/reference_medicines/search/"

        static let activities = "/activities/"
        static let allergies = "/allergies/"
        static let conditions = "/conditions/"
        static let familyConditions = "/familyConditions/"
        static let dependents = "/dependents/"
        static let medicines = "/medicines/"
        static let surgeries = "/surgeries/"

        // Legacy
        static let linkPendingPatient = "/linkPendingPatient"
        static let pendingPatientSearch = "/pendingPatientSearch"
        static let pendingPatientError = "/pendingPatientError"
        static let getPatientAppts = "/getPatientAppts"
        static let getGlobalDoctors = "/getGlobalDoctors"
        static let unlinkDoctor = "/unlinkDoctor"
        static let confirmAppointment = "/confirmAppointment"
        static let cancelAppointment = "/cancelAppointment"
        static let globalPatientExists = "/globalPatientExists"
        static let createGlobalPatient = "/createGlobalPatient"
        static let acceptLinkRequest = "/acceptLinkRequest"
        static let createDependent = "/createDependent"
        static let refreshToken = "/refreshToken"

        static let createPDF = "/createPDF"
        static let getPDF = "/getPDF"
        static let uploadImage = "/uploadImage"

        static let doctorSearch = "/query/doctorSearch?"
        static let getMinimumVersions = "/getMinimumVersions"
    }

    struct MachineLearning {
        static let server = "http://machine-learning.sympliact.aws.logicworks.net"

        struct Paths {
            static let api = "/api"
            static let spellcheck = "/spell-check"
            static let medicine = "/medicine"
            static let condition = "/condition"
            static let surgery = "/surgery"
            static let foodMaterials = "/food-and-materials"
            static let surgeryPrediction = "/surgery-prediction"
            static let medicinesDiseases = "/medicines-to-diseases"
        }
    }
}
