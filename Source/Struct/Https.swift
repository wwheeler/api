//
// Created by ww on 2019/05/13.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case put = "PUT"
}

struct Headers {
    struct Keys {
        static let accept = "Accept"
        static let origin = "Origin"
        static let contentType = "Content-Type"
        static let accessToken = "X-Access-Token"
        static let authorization = "Authorization"
    }

    struct Values {
        static let applicationJson = "application/json"
        static let applicationFormUrlEncoded = "application/x-www-form-urlencoded"
        static let authToken = "Token 5602ed69bdda5829efdbfb2f5c3e4ce0e45a471f"
        static let bearer = "Bearer "
        static let cookie = ""
    }
}

struct Params {
    struct Keys {
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let zip = "zip"
        static let state = "state"
        static let clientId = "client_id"
        static let connection = "connection"
        static let email = "email"
        static let patientId = "patientID"
        static let patientIds = "patientIDs"
    }

    struct Values {
        static let usernamePasswordAuthentication = "Username-Password-Authentication"
        static let whRR4aHT = "whRR4aHT-gIL_EjjnVyp7BrhMvDcCQmY"
    }
}
