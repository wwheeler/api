//
// Created by wwheeler on 2019/04/18.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation

struct Formats {
    static let mmm_dd_yyyy = "MMMM dd, yyyy"
    static let mmm_d_yyyy = "MMM d, yyyy"
    static let yyyy_MM_dd = "yyyy-MM-dd"
    static let yyyy_MM_ddTHHmmss = "yyyy-MM-dd'T'HH:mm:ss"
    static let yyyy_MM_dd_HHmmss_z = "yyyy-MM-dd HH:mm:ss z"
    static let yyyy_MM_ddTHHmmssSSSz = "yyyy-MM-dd'T'HH:mm:ss.SSSz"
}

//    Wednesday, Sep 12, 2018           -> EEEE, MMM d, yyyy
//    09/12/2018                        -> MM/dd/yyyy
//    09-12-2018 14:11                  -> MM-dd-yyyy HH:mm
//    Sep 12, 2:11 PM                   -> MMM d, h:mm a
//    September 2018                    -> MMMM yyyy
//    Sep 12, 2018                      -> MMM d, yyyy
//    Wed, 12 Sep 2018 14:11:54 +0000   -> E, d MMM yyyy HH:mm:ss Z
//    2018-09-12T14:11:54+0000          -> yyyy-MM-dd'T'HH:mm:ssZ
//    12.09.18                          -> dd.MM.yy
//    10:41:02.112                      -> HH:mm:ss.SSS