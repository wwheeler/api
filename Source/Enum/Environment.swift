import Foundation

enum Environment: Int {
    case production = 0
    case staging = 1
    case qa = 2

    static var active: Environment {
        #if QA
        return .qa
        #elseif STAGING
        return .staging
        #else
        return .production
        #endif
    }
}
