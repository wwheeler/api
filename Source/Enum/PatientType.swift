//
// Created by ww on 2019-05-26.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation

enum PatientType: String, Codable {
    case adult = "AdultPatient"
    case child = "ChildPatient"
}

extension PatientType {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let raw = try container.decode(String.self)
        if let value = PatientType(rawValue: raw) {
            self = value
        } else {
            let context = DecodingError.Context(
                    codingPath: decoder.codingPath,
                    debugDescription: "Unexpected raw value \"\(raw)\""
            )
            throw DecodingError.typeMismatch(PatientType.self, context)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.rawValue)
    }
}