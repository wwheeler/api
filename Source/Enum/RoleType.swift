//
// Created by ww on 2019-07-15.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

enum RoleType: String, Codable {
    case patient = "patient"
    case provider = "provider"
    case staff = "staff"
    case admin = "admin"
}

extension RoleType {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let raw = try container.decode(String.self)
        if let value = RoleType(rawValue: raw) {
            self = value
        } else {
            let context = DecodingError.Context(
                    codingPath: decoder.codingPath,
                    debugDescription: "Unexpected raw value \"\(raw)\""
            )
            throw DecodingError.typeMismatch(RoleType.self, context)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.rawValue)
    }
}