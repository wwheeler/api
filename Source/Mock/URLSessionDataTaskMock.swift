//
// Created by ww on 2019-05-15.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation

class URLSessionDataTaskMock: URLSessionDataTaskProtocol {

    private (set) var resuming = false

    func resume() {
        resuming = true
    }
}