//
// Created by ww on 2019-05-15.
// Copyright (c) 2019 Infinity. All rights reserved.
//

import Foundation

class URLSessionMock: URLSessionProtocol {

    var data: Data?
    var error: Error?
    var lastURL: URL?
    var dataTask = URLSessionDataTaskMock()

    func dataTask(
            with request: URLRequest,
            completionHandler: @escaping DataTaskResponse
    ) -> URLSessionDataTaskProtocol {
        lastURL = request.url
        completionHandler(data, httpURLResponse(request: request), error)
        return dataTask
    }

    func httpURLResponse(request: URLRequest) -> URLResponse {
        return HTTPURLResponse(
                url: request.url!,
                statusCode: 200,
                httpVersion: "HTTP/1.1", 
                headerFields: nil
        )!
    }
}