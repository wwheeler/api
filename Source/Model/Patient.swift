//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Patient: Codable {

    var id: String?
    var userPatientId: String?

    var firstName: String = ""
    var lastName: String = ""
    var dateOfBirth: Date = Date()
    var email: String = ""
    var sex: String?

    var ownerType: String = ""
    var isDependent: Bool = false
    var needsMerge: Bool = false

    var middleName: String?
    var phoneNum: String? = ""
    var secondaryPhoneNum: String? = ""
    var workPhone: String? = ""

    var height: Int?
    var weight: Int?
    var imageURL: String?

    var address: Address?
    var driversLicense: DriversLicense?
    var emergencyContacts: [EmergencyContact]? = []
    var insuranceCards: [InsuranceCard]? = []
    var dependents: [Patient] = []

    var activities: [Activity] = []
    var allergies: [Allergy] = []
    var conditions: [Condition] = []
    var familyConditions: [Condition]? = []
    var medicines: [Medicine] = []
    var surgeries: [Surgery] = []

//    var badgeNumber: Int?
//    var imageURL: String?
//    var fcmToken: [String]?
//
//    var phoneVerified: Bool?
//    var emailVerified: Bool?
//
//    var hipaaDone: Bool?
//    var noPPDone: [Bool]?
//
//    var healthHistoryProgress: [String]?
//    var demographicProgress: [String]?
//
//    var linkedPersonIDs: [String]?
//    var doctorIDs: [String]?
//    var practiceIDs: [String]?
//    var appointmentIDs: [String]?

}