//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Medicine: Codable {

    var id: String?

    var genericName: String = ""
    var tradeName: String = ""
    var doseForm: String = ""

    var active: Bool? = false
    var dateAdded: Date?
    var dateRemoved: Date?

}
