//
// Created by ww on 2019-07-12.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Query: Codable {

    var nameCont: String?
    var emailCont: String?
    var tradeNamesCont: String?
    var genericNameCont: String?

    init(
            nameCont: String? = nil,
            emailCont: String? = nil,
            tradeNamesCont: String? = nil,
            genericNameCont: String? = nil
    ) {
        self.nameCont = nameCont
        self.emailCont = emailCont
        self.tradeNamesCont = tradeNamesCont
        self.genericNameCont = genericNameCont
    }
}
