//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct NoteModel: Codable {

    var id: String?

    var patientID: String?
    var date: Date?
    var type: String?
    var data: String?

}
