//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Activity: Codable {

    var id: String?

    var name: String = ""
    var drugName: String = ""

    var frequency: String = ""
    var quantity: String = ""
    var units: String = ""

    var lastUsed: String = ""
    var yearsUsed: Int = 0

    var active: Bool? = false
    var dateAdded: Date?
    var dateRemoved: Date?

}