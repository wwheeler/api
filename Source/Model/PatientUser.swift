//
// Created by ww on 2019-07-16.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct PatientUser: Codable {

    var id: String?
    var userPatientId: String?

    var firstName: String = ""
    var lastName: String = ""
    var dateOfBirth: Date = Date()
    var email: String = ""
    var sex: String?

    var ownerType: String = ""
    var isDependent: Bool = false
    var needsMerge: Bool = false

    init(
            id: String?,
            userPatientId: String?,
            firstName: String,
            lastName: String,
            dateOfBirth: Date,
            email: String,
            sex: String?,
            ownerType: String,
            isDependent: Bool,
            needsMerge: Bool
    ) {
        self.id = id
        self.userPatientId = userPatientId
        self.firstName = firstName
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.email = email
        self.sex = sex
        self.ownerType = ownerType
        self.isDependent = isDependent
        self.needsMerge = needsMerge
    }
}
