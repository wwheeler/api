//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Surgery: Codable {

    var id: String?

    var laymanName: String = ""
    var professionalName: String = ""

    var active: Bool? = false
    var dateAdded: Date?
    var dateRemoved: Date?

}