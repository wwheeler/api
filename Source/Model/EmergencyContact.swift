//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct EmergencyContact: Codable, Equatable {

    var id: String?

    var name: String = ""
    var relation: String = ""
    var phoneNum: String = ""

    static func == (lhs: EmergencyContact, rhs: EmergencyContact) -> Bool {
        return lhs.name == rhs.name && lhs.relation == rhs.relation && lhs.phoneNum == rhs.phoneNum
    }
}
