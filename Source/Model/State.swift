//
// Created by ww on 2019-06-20.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct State: Codable {

    var id: String?

    var legal: [String] = ["", ""]
    var demographic: [String] = ["", "", "", "", "", "", ""]
    var healthHistory: [String] = ["", "", "", "", "", "", "", ""]
    var progress: [String] = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]

}
