//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct DriversLicense: Codable {

    var id: String = ""
    var state: String = ""
    var DLNumber: String = ""

    var frontImageURL: String = ""
    var backImageURL: String = ""

}
