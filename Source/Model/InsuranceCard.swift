//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct InsuranceCard: Codable, Equatable {

    var id: String?

    var insuranceCompany: String = ""
    var insurancePhoneNum: String = ""

    var insuranceType: String = ""
    var employerPhoneNum: String = ""
    var identificationNum: String = ""
    var groupNum: String = ""

    var frontImageURL: String = ""
    var backImageURL: String = ""

    var subscriberName: String = ""
    var subscriberRelationship: String = ""
    var subscriberEmployer: String = ""

    var subscriberDOB: Date = Date()
    var subscriberAddress: Address = Address()

    //todo more equatable checks
    static func == (lhs: InsuranceCard, rhs: InsuranceCard) -> Bool {
        return lhs.subscriberName == rhs.subscriberName
    }
}
