//
//  PatientModel.swift
//  Sympliact
//
//  Created by ww on 2019/05/24.
//  Copyright © 2019 Sympliact. All rights reserved.
//

import Foundation

struct PatientModel: Codable {

    var id: String?
    var patient: PatientUser?

    init(id: String? = nil, patient: PatientUser? = nil) {
        self.id = id
        self.patient = patient
    }
}
