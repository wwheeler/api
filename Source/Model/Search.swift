//
// Created by ww on 2019-07-12.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Search: Codable {

    var q: Query?

    init(q: Query? = nil) {
        self.q = q
    }
}
