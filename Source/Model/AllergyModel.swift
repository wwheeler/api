//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct AllergyModel: Codable {

    var patientId: String?
    var allergy: Allergy

    init(patientId: String? = "", allergy: Allergy) {
        self.patientId = patientId
        self.allergy = allergy
    }
}
