//
// Created by ww on 2019-06-21.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct User: Codable {

    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var password: String = ""
    var dateOfBirth: Date = Date()
    var appMetadata: Role = Role(sympliactRole: .patient)
}

struct Role: Codable {

    var sympliactRole: RoleType?
}
