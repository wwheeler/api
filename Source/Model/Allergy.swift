//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Allergy: Codable {

    var id: String?

    var name: String = ""
    var allergyType: String = ""

    var active: Bool? = false
    var dateAdded: Date?
    var dateRemoved: Date?

    var notes: [NoteModel]?
}
