//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Address: Codable {

    var id: String?

    var street1: String = ""
    var street2: String = ""
    var city: String = ""
    var state: String = ""
    var postal: String = ""

}
