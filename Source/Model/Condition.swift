//
// Created by ww on 2019/04/29.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

struct Condition: Codable {

    var id: String?

    var laymanName: String = ""
    var professionalName: String = ""

}