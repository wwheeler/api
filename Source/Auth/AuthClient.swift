//
// Created by ww on 2019/04/27.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Auth0
import LocalAuthentication
import Foundation

var bearerToken = ""

class AuthClient {

    // MARK: - Shared Instance

    static let shared = AuthClient()

    // MARK: - Properties

    private let authContext = LAContext()
    private var credentialsManager = CredentialsManager(authentication: Auth0.authentication())

    private let apiClient = ApiClient.shared
    private let apiUsersUrl = Api.server + Api.Paths.pat + Api.Paths.users

    private let scope = "openid profile offline_access"
    private let userPassAuth = "Username-Password-Authentication"
    private let apiIdentifier = "https://sympliact.auth0.com/api/v2/"

    // MARK: Lifecycle

    init() {
        //todo add
        // guard NetworkUtil.connectedToNetwork() else
    }

    // MARK: - Operations

    func signUp(
            user: User,
            completion: @escaping (Response?) -> Void,
            failure: @escaping (Error?) -> Void
    ) {
        if let model = JSONEncoder().encode(value: UserModel(user: user)) {
            let urlRequest = URLRequest(
                    urlString: apiUsersUrl,
                    httpMethod: .post,
                    httpBody: model
            )
            apiClient.request(urlRequest: urlRequest!, completion: { data in
                if let temp = JSONDecoder().decode(type: Response.self, data: data!) {
                    completion(temp)
                }
            }, failure: { error in
                print("> AuthClient/signUp/error: \(error!))")
                failure(error)
            })
        }
    }

    func signIn(
            email: String,
            password: String,
            completion: @escaping (Credentials?) -> Void,
            failure: @escaping (Error?) -> Void
    ) {
        Auth0.authentication().login(
                usernameOrEmail: email.lowercased(),
                password: password,
                realm: userPassAuth,
                audience: apiIdentifier,
                scope: scope
        ).start {
            switch $0 {
            case .success(let credentials):
                self.credentialsManager.store(credentials: credentials)
                bearerToken = "Bearer \(String(credentials.accessToken!))"
                print("\(bearerToken)")
                completion(credentials)
            case .failure(let error):
                print("> AuthClient/signIn/error: \(error)")
                failure(error)
            }
        }
    }

    func signIn(completion: @escaping (Bool) -> Void) {
        signInWithBiometric(completion: { success in
            completion(success)
        })
    }

    //todo move to session manager for bearertoken
    func refresh(
            completion: @escaping (Credentials) -> Void,
            failure: @escaping (Error) -> Void
    ) {
        // If the credentials have expired, the credentials manager
        // will automatically renew them for you with the Refresh Token.
        // There is no need to store the credentials as you did in Login.
        // The Credentials Manager will do this for you internally
        guard credentialsManager.hasValid() else {
            print("> AuthClient/refresh/error: Credentials not valid")
            return
        }
        credentialsManager.credentials { error, credentials in
            if let credentials = credentials {
                bearerToken = "Bearer \(String(credentials.accessToken!))"
                print("\(bearerToken)")
                completion(credentials)
            } else if let error = error {
                print("> AuthClient/refresh/error: \(error)")
                failure(error)
            }
        }
    }

    // MARK: - Private Operations

    //todo biometric checks passcode/finger/face own manager

    private func signInWithBiometric(completion: @escaping (_ success: Bool) -> Void) {
        guard isBiometricsEnabled() else {
            print("> AuthClient/signInWithBiometric/error: Biometrics not enabled")
            credentialsManager.enableBiometrics(withTitle: "Touch to Authenticate")
            return completion(false)
        }
        authContext.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Use Finger Print",
                reply: { success, error in
                    if success {
                        completion(success)
                    } else if let error = error {
                        print("> AuthClient/signInWithBiometric/error: \(error.localizedDescription)")
                        completion(false)
                    }
                }
        )
    }

    private func isBiometricsEnabled() -> Bool {
        return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
}
