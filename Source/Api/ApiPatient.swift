//
// Created by ww on 2019/04/27.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation

class ApiPatient {

    // MARK: - Shared Instance

    static let shared = ApiPatient()

    // MARK: - Properties

    private var apiClient = ApiClient.shared
    private var apiClientSession: URLSessionProtocol?

    private var apiPatUrl: String
    private var apiUsersUrl: String
    private var apiProfilesUrl: String
    private var apiPatientsUrl: String
    private var apiMedicinesSearchUrl: String

    var urlSession: URLSessionProtocol {
        set(val) {
            apiClient.urlSession = val
        }
        get {
            return self.apiClientSession!
        }
    }

    // MARK: Lifecycle

    init() {
        apiPatUrl = Api.server + Api.Paths.pat
        apiUsersUrl = apiPatUrl + Api.Paths.users
        apiProfilesUrl = apiPatUrl + Api.Paths.profiles
        apiPatientsUrl = apiPatUrl + Api.Paths.patients
        apiMedicinesSearchUrl = apiPatUrl + Api.Paths.searchMedicines
    }

    // MARK: - User Operations

    func create(user: User, completion: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        if let data = JSONEncoder().encode(value: UserModel(user: user)) {
            let urlRequest = URLRequest(
                    urlString: apiUsersUrl,
                    httpMethod: .post,
                    httpBody: data
            )
            apiClient.request(type: User.self, urlRequest: urlRequest!, completion: completion, failure: failure)
        }
    }

    // MARK: - Profile Operations

    func get(completion: @escaping (Patient) -> Void, failure: @escaping (Error) -> Void) {
        let urlRequest = URLRequest(
                urlString: apiProfilesUrl,
                httpMethod: .get,
                httpHeaders: [
                    Headers.Keys.authorization: bearerToken
                ]
        )
        apiClient.request(type: Patient.self, urlRequest: urlRequest!, completion: completion, failure: failure)
    }

    // MARK: - Patient Operations

    func get(patientId: String, completion: @escaping (Patient) -> Void, failure: @escaping (Error) -> Void) {
        let urlRequest = URLRequest(
                urlString: apiPatientsUrl + patientId,
                httpMethod: .get,
                httpHeaders: [
                    Headers.Keys.authorization: bearerToken
                ]
        )
        apiClient.request(type: Patient.self, urlRequest: urlRequest!, completion: completion, failure: failure)
    }

    func update(patient: Patient, completion: @escaping (PatientUser) -> Void, failure: @escaping (Error) -> Void) {
        let patientUser = PatientUser(
                id: patient.id,
                userPatientId: patient.userPatientId,
                firstName: patient.firstName,
                lastName: patient.lastName,
                dateOfBirth: patient.dateOfBirth,
                email: patient.email,
                sex: patient.sex,
                ownerType: patient.ownerType,
                isDependent: patient.isDependent,
                needsMerge: patient.needsMerge
        )
        if let data = JSONEncoder().encode(value: PatientModel(patient: patientUser)) {
            let urlRequest = URLRequest(
                    urlString: apiPatientsUrl + patient.id!,
                    httpMethod: .put,
                    httpHeaders: [
                        Headers.Keys.authorization: bearerToken
                    ],
                    httpBody: data
            )
            apiClient.request(type: PatientUser.self, urlRequest: urlRequest!, completion: completion, failure: failure)
        }
    }

    // MARK: - Search Operations

    func search(medicine: String, completion: @escaping ([Medicine]) -> Void, failure: @escaping (Error) -> Void) {
        if let data = JSONEncoder().encode(value: Search(q: Query(tradeNamesCont: medicine))) {
            let urlRequest = URLRequest(
                    urlString: apiMedicinesSearchUrl,
                    httpMethod: .post,
                    httpBody: data
            )
            apiClient.request(type: [Medicine].self, urlRequest: urlRequest!, completion: { data in
                completion(data)
            }, failure: failure)
        }
    }

    // MARK: - Activities Operations

    func update(activity: Activity, patientId: String, completion: @escaping (Activity) -> Void, failure: @escaping (Error) -> Void) {
        if let data = JSONEncoder().encode(value: ActivityModel(patientId: patientId, activity: activity)) {
            let urlRequest = URLRequest(
                    urlString: apiPatientsUrl + patientId + Api.Paths.activities,
                    httpMethod: .put,
                    httpBody: data
            )
            apiClient.request(type: Activity.self, urlRequest: urlRequest!, completion: completion, failure: failure)
        }
    }

    // MARK: - Allergies Operations

    func update(allergy: Allergy, patientId: String, completion: @escaping (Allergy) -> Void, failure: @escaping (Error) -> Void) {
        if let data = JSONEncoder().encode(value: AllergyModel(patientId: patientId, allergy: allergy)) {
            let urlRequest = URLRequest(
                    urlString: apiPatientsUrl + patientId + Api.Paths.activities,
                    httpMethod: .put,
                    httpBody: data
            )
            apiClient.request(type: Allergy.self, urlRequest: urlRequest!, completion: completion, failure: failure)
        }
    }

    // MARK: - Conditions Operations
    // MARK: - FamilyConditions Operations
    // MARK: - Dependents Operations
    // MARK: - Medicines Operations
    // MARK: - Surgeries Operations

    func create<T: Codable>(item: T, patientId: String, completion: @escaping (T) -> Void) {
        let urlRequest = URLRequest(
                urlString: apiPatientsUrl + patientId + getPath(item: item),
                httpMethod: .post,
                httpBody: getData(item: item)
        )
        apiClient.request(type: T.self, urlRequest: urlRequest!, completion: completion, failure: { error in
            print("> ApiPatient/create/item/error: \(item) \(error.localizedDescription)")
        })
    }

    func delete<T: Codable>(item: T, itemId: String) {
        let urlRequest = URLRequest(
                urlString: apiPatUrl + getPath(item: item) + itemId,
                httpMethod: .delete
        )
        apiClient.request(type: T.self, urlRequest: urlRequest!, completion: { data in }, failure: { error in
            print("> ApiPatient/delete/item/error: \(item) \(error.localizedDescription)")
        })
    }

    private func getPath<T>(item: T) -> String {
        switch item {
        case is Activity:
            return Api.Paths.activities
        case is Allergy:
            return Api.Paths.allergies
        case is Condition:
            return Api.Paths.conditions
        case is Medicine:
            return Api.Paths.medicines
        default:
            return Api.Paths.surgeries
        }
    }

    private func getData<T>(item: T) -> Data {
        switch item {
        case is Activity:
            return JSONEncoder().encode(value: ActivityModel(activity: item as! Activity))!
        case is Allergy:
            return JSONEncoder().encode(value: AllergyModel(allergy: item as! Allergy))!
        case is Condition:
            return JSONEncoder().encode(value: ConditionModel(condition: item as! Condition))!
        case is Medicine:
            return JSONEncoder().encode(value: MedicineModel(medicine: item as! Medicine))!
        default:
            return JSONEncoder().encode(value: SurgeryModel(surgery: item as! Surgery))!
        }
    }

}
