//
// Created by ww on 2019-05-15.
// Copyright (c) 2019 Sympliact. All rights reserved.
//

import Foundation
import UIKit

class ApiClient {

    // MARK: - Shared Instance

    static let shared = ApiClient()

    // MARK: - Properties

    internal var urlSession: URLSessionProtocol

    // MARK: Lifecycle

    init() {
        print("> ApiClient/Environment:", Environment.active)
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.httpAdditionalHeaders = [
            Headers.Keys.accept: Headers.Values.applicationJson,
            Headers.Keys.contentType: Headers.Values.applicationJson
        ]
        self.urlSession = URLSession(configuration: urlSessionConfiguration)
    }

    // MARK: - Operations

    func request(
            urlString: String,
            completion: @escaping (String?) -> Void
    ) {
        let request = URLRequest(
                urlString: urlString,
                httpMethod: .get
        )
        self.request(urlRequest: request!, completion: { data in
            let json = String(data: data!, encoding: .utf8)
            completion(json)
        }, failure: { error in })
    }

    func request<T: Codable>(
            type: T.Type,
            urlRequest: URLRequest,
            completion: @escaping (T) -> Void,
            failure: @escaping (Error) -> Void
    ) {
        self.request(urlRequest: urlRequest, completion: { data in
            if let model = JSONDecoder().decode(type: T.self, data: data!) {
                completion(model)
            }
        }, failure: { error in
            failure(error!)
        })
    }

    func request(
            urlRequest: URLRequest,
            completion: @escaping (Data?) -> Void,
            failure: @escaping (Error?) -> Void
    ) {
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("> ApiClient/error: \(error)")
                failure(error)
            } else if let data = data, let response = response as? HTTPURLResponse {
                if 200...299 ~= response.statusCode {
                    completion(data)
                } else {
                    let responseError = NSError(
                            domain: "",
                            code: response.statusCode,
                            userInfo: [NSLocalizedDescriptionKey: HTTPURLResponse.localizedString(forStatusCode: response.statusCode)]
                    ) as Error
                    print("> ApiClient/response/error: \(responseError)")
                    failure(responseError)
                }
            }
        }
        task.resume()
    }
}

protocol URLSessionProtocol {
    typealias DataTaskResponse = (Data?, URLResponse?, Error?) -> Void
    func dataTask(
            with request: URLRequest,
            completionHandler: @escaping DataTaskResponse
    ) -> URLSessionDataTaskProtocol
}

extension URLSession: URLSessionProtocol {
    func dataTask(
            with request: URLRequest,
            completionHandler: @escaping DataTaskResponse
    ) -> URLSessionDataTaskProtocol {
        return dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask
    }
}

protocol URLSessionDataTaskProtocol {
    func resume()
}

extension URLSessionDataTask: URLSessionDataTaskProtocol {
}
