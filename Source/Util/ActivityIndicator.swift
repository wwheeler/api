import Foundation
import UIKit

class ActivityIndicator {

    static var indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
    static var container: UIView = UIView()
    static var loading: UIView = UIView()

    static func showActivityIndicator(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.fromHex(rgbValue: 0xffffff, alpha: 0.8)

        loading.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loading.center = uiView.center
        loading.backgroundColor = UIColor.fromHex(rgbValue: 0x444444, alpha: 0.7)
        loading.clipsToBounds = true
        loading.layer.cornerRadius = 10

        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = CGPoint(x: loading.frame.size.width / 2, y: loading.frame.size.height / 2);

        loading.addSubview(indicator)
        container.addSubview(loading)

        uiView.addSubview(container)
        indicator.startAnimating()
    }

    static func hideActivityIndicator(uiView: UIView) {
        indicator.stopAnimating()
        container.removeFromSuperview()
    }

}